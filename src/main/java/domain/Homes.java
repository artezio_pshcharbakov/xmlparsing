package domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "homes")
public class Homes {

    private List<Apartment> apartments;

    public Homes() {
        apartments = new ArrayList<>();
    }


    @XmlElement
    public List<Apartment> getApartments() {
        return apartments;
    }

    public void addApartment(Apartment apar) {
        apartments.add(apar);
    }

    public void setApartments(List<Apartment> apartments) {
        this.apartments = apartments;
    }

    @Override
    public String toString() {
        return "Homes{" + apartments + '}';
    }
}
