package domain;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum LoftType {

    ANCIENT("ancient loft with spiders"),

    CLEAN("safety and clean loft");

    private String description;

    LoftType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
