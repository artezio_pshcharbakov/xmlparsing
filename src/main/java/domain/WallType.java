package domain;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum WallType {

    Wallpaper("обои"),

    Paint("окрашенные стены"),

    Decor("другой вид");

    private String description;

    WallType(String type) {
        description = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
