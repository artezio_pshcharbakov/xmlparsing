package enterprise;

import domain.Homes;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Main {

    private final static String sourceXml = "src/main/resources/Homes.xml";
    private final static String xsdSchema = "src/main/resources/definition.xsd";

    public static void main(String[] args) {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        Service<Homes> service = container.instance().select(Service.class).get();
        Homes h = service.getHomesFromXml(sourceXml);
        weld.shutdown();
    }
}

