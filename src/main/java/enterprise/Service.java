package enterprise;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Loggable
@Stateless
class Service<T> {

    @Inject
    @DOM
    private Parseble<T> parser;

    T getHomesFromXml(String inputXml) {
        return parser.parse(inputXml);
    }
}
