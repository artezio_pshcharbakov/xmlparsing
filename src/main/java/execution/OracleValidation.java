package execution;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class OracleValidation {

    public void validate(String xmlFile, String xsdFile)throws IOException, ParserConfigurationException, SAXException {

        //parse an XML document into a DOM tree
        DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = parser.parse(new File(xmlFile));
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        //create a SchameFactory capable by a Schema instance
        Source schemaFile = new StreamSource(new File(xsdFile));
        Schema schema = factory.newSchema(schemaFile);

        //create a Validator instance, which can be used to validate an instance document
        Validator validator = schema.newValidator();

        try{
            validator.validate(new DOMSource(document));
            System.out.println("document valid");
        } catch (SAXException | IOException e){
            e.printStackTrace();
        }
    }
}
