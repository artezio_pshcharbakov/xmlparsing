/**
 *  Здесь используется StAX парсер, применяя  <code>XMLStreamReader</code>, в отличие от класса
 *  StAXEvents, где используется <code>XMLEventRreader</code>
 */
package execution;

import domain.Address;
import domain.Apartment;
import domain.Homes;
import domain.LoftType;
import domain.Room;
import domain.WallType;
import enterprise.Parseble;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

public class StAXParserDemo implements Parseble<Homes> {

    private Homes homes;
    private Room room;
    private int windows;
    private int doors;
    private WallType wall;
    private Address address;
    private LoftType loft;
    private String element;

    public Homes parse(String inputFile) {

        XMLStreamReader reader = null;
        try {
            reader = XMLInputFactory.newInstance()
                    .createXMLStreamReader(new FileReader(inputFile));
            int event = reader.getEventType();
            while (reader.hasNext()) {
                switch (event) {
                    case XMLStreamConstants.START_DOCUMENT:
                        homes = new Homes();
                        break;
                    case XMLStreamConstants.START_ELEMENT:
                        element = reader.getLocalName();
                        if (element.equalsIgnoreCase("address")) {
                            address = new Address();
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        if (reader.isWhiteSpace())
                            break;
                        if ("city".equals(element)) {
                            address.setCity(reader.getText());
                        } else if ("street".equals(element)) {
                            address.setStreet(reader.getText());
                        } else if ("number".equals(element)) {
                            address.setNumber(Integer.parseInt(reader.getText()));
                        } else if ("type".equals(element)) {
                            wall = WallType.valueOf(reader.getText());
                        } else if ("windows".equals(element)) {
                            windows = Integer.parseInt(reader.getText());
                        } else if ("doors".equals(element)) {
                            doors = Integer.parseInt(reader.getText());
                            room = new Room.Builder(wall).doors(doors).windows(windows).build();//TODO костыль
                        } else if ("loft".equals(element)) {
                            loft = LoftType.valueOf(reader.getText());
                            homes.addApartment(createApartment());//Todo костыль
                        }
                        break;
                        //TODO It doesn't work.
                    case XMLStreamConstants.END_ELEMENT:
                        if ("rooms".equals(element)) {
                            room = new Room.Builder(wall).doors(doors).windows(windows).build();
                        } else if ("apartments".equals(element)) {
                            homes.addApartment(createApartment());
                        }
                        break;
                }
                event = reader.next();
            }
        } catch (XMLStreamException | IOException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (XMLStreamException e){
                System.err.println(e.getMessage());
            }
        }
        return homes;
    }

    private Apartment createApartment() {
        Apartment apartment = new Apartment();
        apartment.setAddress(address);
        apartment.setRooms(Collections.singletonList(room));
        apartment.setLoft(loft);
        return apartment;
    }
}
