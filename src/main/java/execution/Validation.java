//Old validation since last-year courses
package execution;

import Exceptions.ErrorReg;
import enterprise.Loggable;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

@Loggable
@Stateless
public class Validation {

    public void validate(String xmlFile, String xsdFile) {
        boolean flag = false;
        SchemaFactory schFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        ErrorHandler error = new ErrorReg();

        try {
            Validator val = schFactory.newSchema(new File(xsdFile)).newValidator();
            Source source = new StreamSource(new File(xmlFile));
            val.validate(source);
            val.setErrorHandler(error);
            flag = true;
        } catch (SAXException | IOException e) {
            System.err.println(e.getMessage());
        }
        System.out.println("xml file is valid: " + flag);
    }

    @PostConstruct
    public void result() {
        System.err.println("GOOGOGOGOGOGOGOGO");
    }
}
