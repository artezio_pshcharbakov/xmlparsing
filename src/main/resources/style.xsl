<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/homes">
        <html>
            <body>
                <h3>All houses</h3>
                <table border="1">
                    <tr bgcolor="#93cd32">
                        <th>Adress</th>
                        <th>Rooms</th>
                        <th>Loft Type</th>
                    </tr>
                    <xsl:for-each select="apartments">
                        <xsl:sort order="ascending" case-order="lower-first"
                                  select="rooms/type"/>
                        <tr>
                            <td><xsl:value-of select="address/city"/></td>
                            <td><xsl:value-of select="rooms/type"/></td>
                            <td><xsl:value-of select="loft"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>